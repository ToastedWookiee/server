package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/controllercubiomes/server/db"
	"gitlab.com/controllercubiomes/util"
)

func IdMiddleware(dbc db.DB) gin.HandlerFunc {
	return func(c *gin.Context) {
		token := c.Request.Header.Get("authorization")
		if err := dbc.SystemKeyExists(token); err != nil {
			err := c.AbortWithError(400, fmt.Errorf("Your request cannot be ided and will be discarded: %w", err))
			if err != nil {
				log.Println(err)
			}
			return
		}
		c.Next()
	}
}

func AnaMiddleware(dbc db.DB) gin.HandlerFunc {
	return func(c *gin.Context) {
		token := c.Request.Header.Get("authorization")
		if err := dbc.SystemAnalyzeKeyExists(token); err != nil {
			err := c.AbortWithError(400, fmt.Errorf("Your request cannot be ided and will be discarded: %w", err))
			if err != nil {
				log.Println(err)
			}
			return
		}
		c.Next()
	}
}

func main() {
	dbCred := db.DBCred{
		DB_HOST:     os.Getenv("POSTGRES_HOST"),
		DB_USER:     os.Getenv("POSTGRES_USER"),
		DB_NAME:     os.Getenv("POSTGRES_DB"),
		DB_PASSWORD: os.Getenv("POSTGRES_PASSWORD"),
	}
	conn, err := dbCred.ConnectToDB()
	if err != nil {
		log.Fatal(err)
	}
	dbc := db.DB{Conn: conn}
	ticker := time.NewTicker(1 * time.Hour)
	go func() {
		for range ticker.C {
			if err := dbc.CleanupChunks(); err != nil {
				log.Println(err)
			}
		}
	}()
	r := gin.Default()
	ided := r.Group("/")
	ided.Use(IdMiddleware(dbc))
	{
		ided.GET("controller/chunk/:crit", func(c *gin.Context) {
			chunk, err := dbc.GetFreeChunk(c.Request.Header.Get("authorization"), c.Param("crit"))
			if err != nil {
				log.Println(err)
				c.JSON(500, gin.H{"Error": err})
				return
			}
			log.Println(chunk)
			if err := json.NewEncoder(c.Writer).Encode(chunk); err != nil {
				log.Println(err)
			}
		})
		ided.POST("controller/chunkdone", func(c *gin.Context) {
			var chunk util.Chunk
			if err := c.BindJSON(&chunk); err != nil {
				c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
				return
			}
			log.Println(chunk)
			if err := dbc.ChunkDone(chunk); err != nil {
				log.Println(err)
			}
		})
		ided.POST("controller/seed", func(c *gin.Context) {
			var seed util.Initial
			if err := c.BindJSON(&seed); err != nil {
				c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
				return
			}
			if err := dbc.RegisterSeed(seed, c.Request.Header.Get("authorization")); err != nil {
				log.Println(err)
			}
		})
	}
	ana := r.Group("/")
	ana.Use(AnaMiddleware(dbc))
	{
		ana.GET("controller/analyzechunk", func(c *gin.Context) {
			chunk, err := dbc.GetAnalyzerChunk()
			if err != nil {
				log.Println(err)
				c.JSON(500, gin.H{"Error": err})
				return
			}
			if err := json.NewEncoder(c.Writer).Encode(chunk); err != nil {
				log.Println(err)
			}
		})
		ana.POST("controller/analyzedseed", func(c *gin.Context) {
			var seed util.Seed
			if err := c.BindJSON(&seed); err != nil {
				c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
				return
			}
			if err := dbc.UpdateSeed(seed); err != nil {
				log.Println(err)
			}
		})
		ana.POST("controller/falsepositive", func(c *gin.Context) {
			var seed util.Seed
			if err := c.BindJSON(&seed); err != nil {
				c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
				return
			}
			if err := dbc.FalsePositive(seed); err != nil {
				log.Println(err)
			}
		})
	}
	if err := r.Run(":3030"); err != nil {
		log.Println(err)
	}
}
