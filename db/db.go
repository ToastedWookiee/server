package db

import (
	"database/sql"
	"fmt"
	"log"
	"time"

	_ "github.com/lib/pq"
	"gitlab.com/controllercubiomes/util"
)

type DBCred struct {
	DB_HOST     string
	DB_USER     string
	DB_PASSWORD string
	DB_NAME     string
}

type DB struct {
	Conn *sql.DB
}

func (dbc DBCred) ConnectToDB() (*sql.DB, error) {
	dbinfo := ""
	if dbc.DB_PASSWORD == "" {
		dbinfo = fmt.Sprintf("host=%s user=%s dbname=%s sslmode=disable",
			dbc.DB_HOST, dbc.DB_USER, dbc.DB_NAME)
	} else {
		dbinfo = fmt.Sprintf("host=%s user=%s password=%s dbname=%s sslmode=disable",
			dbc.DB_HOST, dbc.DB_USER, dbc.DB_PASSWORD, dbc.DB_NAME)
	}

	db, err := sql.Open("postgres", dbinfo)
	if err != nil {
		return nil, err
	}

	return db, db.Ping()
}

func (db DB) RegisterSeed(seed util.Initial, key string) (err error) {
	result, err := db.Conn.Exec("INSERT INTO seeds (seed, key, verified) VALUES ($1, $2, $3);", seed.Seed, key, false)
	if err != nil {
		return
	}
	rowsAff, err := result.RowsAffected()
	if rowsAff < 1 || err != nil {
		if err == nil {
			err = fmt.Errorf("Database error")
		}
		return
	}
	result, err = db.Conn.Exec("INSERT INTO SeedMatchesCriteria (seed, criteria, verified) VALUES ($1, $2, $3);", seed.Seed, seed.Criteria, false)
	if err != nil {
		return
	}
	rowsAff, err = result.RowsAffected()
	if rowsAff < 1 || err != nil {
		if err == nil {
			err = fmt.Errorf("Database error")
		}
		return
	}
	return
}

func (db DB) UpdateSeed(seed util.Seed) (err error) {
	seed.GINI = 1.1
	seed.Ocean.Total = sumOcean(seed.Ocean)
	seed.Mesa.Total = sumMesa(seed.Mesa)
	seed.Desert.Total = sumDesert(seed.Desert)
	seed.Savanna.Total = sumSavanna(seed.Savanna)
	seed.Plains.Total = sumPlains(seed.Plains)
	seed.Swamp.Total = sumSwamp(seed.Swamp)
	seed.Forest.Total = sumForest(seed.Forest)
	seed.Roofed.Total = sumRoofed(seed.Roofed)
	seed.Mountain.Total = sumMountain(seed.Mountain)
	seed.Mushroom.Total = sumMushroom(seed.Mushroom)
	seed.Jungle.Total = sumJungle(seed.Jungle)
	seed.Taiga.Total = sumTaiga(seed.Taiga)
	seed.MegaTaiga.Total = sumMegaTaiga(seed.MegaTaiga)
	seed.Snowy.Total = sumSnowy(seed.Snowy)
	result, err := insertSeed(db.Conn, seed)
	if err != nil {
		return
	}
	rowsAff, err := result.RowsAffected()
	if rowsAff < 1 || err != nil {
		if err == nil {
			err = fmt.Errorf("Database error")
		}
		return
	}
	return
}

func (db DB) SeedVerified(seed util.Seed) (err error) {
	result, err := db.Conn.Exec("UPDATE seeds SET verified = $1 WHERE seed = $2;", true, seed.Seed)
	if err != nil {
		return
	}
	rowsAff, err := result.RowsAffected()
	if rowsAff < 1 || err != nil {
		if err == nil {
			err = fmt.Errorf("Database error")
		}
		return
	}
	return
}

func (db DB) DeleteSeed(seed util.Seed) (err error) {
	result, err := db.Conn.Exec("DELETE FROM seeds WHERE seed = $2;", true, seed.Seed)
	if err != nil {
		return
	}
	rowsAff, err := result.RowsAffected()
	if rowsAff < 1 || err != nil {
		if err == nil {
			err = fmt.Errorf("Database error")
		}
		return
	}
	return
}

func (db DB) GetAnalyzerChunk() (chunk util.AnalyzeChunk, err error) {
	chunk.RadiusInner = 1500 // Not sure how to handle this int the future. Need more info about other criterias
	chunk.RadiusOuter = 2048 // Not sure how to handle this int the future. Need more info about other criterias
	chunk.ProgramURL = "https://gitlab.com/controllercubiomes/core/-/jobs/artifacts/master/raw/<file>?job=build"
	rows, err := db.Conn.Query("SELECT seed FROM seeds WHERE verified = FALSE ORDER BY random() LIMIT 10;")
	if err != nil {
		err = fmt.Errorf("GetAnalyzerChunk: Err: %w", err)
		return
	}
	for rows.Next() {
		var seed int64
		if err := rows.Scan(&seed); err != nil {
			log.Println("Getting analyzer seed: ", err)
		}
		chunk.Seeds = append(chunk.Seeds, seed)
	}
	return
}

func (db DB) GetFreeChunk(key, criteria string) (chunk util.Chunk, err error) {
	chunk = util.Chunk{
		Key: key,
	}
	err = db.Conn.QueryRow("SELECT innerRadius, outerRadius, programURL FROM criterias WHERE name = $1;", criteria).Scan(&chunk.RadiusInner, &chunk.RadiusOuter, &chunk.ProgramURL)
	if err != nil {
		err = fmt.Errorf("Criteria: %s, Err: %w", criteria, err)
		return
	}

	err = db.Conn.QueryRow("UPDATE chunks SET verifier = $1, verStartTime = $2 WHERE id IN (SELECT id FROM chunks WHERE criteria = $3 AND key != $1 AND seedsFound IS NOT NULL AND verifier IS NULL LIMIT 1) RETURNING id, startSeed, endSeed;", key, time.Now(), criteria).Scan(&chunk.ID, &chunk.Start, &chunk.End)
	if err == nil && chunk.Start != chunk.End {
		return
	}

	if err != sql.ErrNoRows {
		log.Println(fmt.Errorf("Verification Criteria: %s, Err: %w", criteria, err))
	}
	rows, err := db.Conn.Query("SELECT startSeed, endSeed FROM chunks WHERE criteria = $1 ORDER BY startSeed ASC;", criteria) //TODO: if the database gets unresponsive move ordering to the backend instead
	if err != nil {
		err = fmt.Errorf("Getting chunks for criteria: %s, Err: %w", criteria, err)
		return
	}
	chunkSize := int64(1 << 30)
	var lastEnd int64
	defer rows.Close()
	for rows.Next() {
		var start int64
		var end int64
		err = rows.Scan(&start, &end)
		if err != nil {
			err = fmt.Errorf("Scanning rows: %w", err)
			chunk = util.Chunk{}
			return
		}
		if lastEnd+chunkSize <= start {
			break
		}
		lastEnd = end

	}
	if lastEnd == 0 {
		chunk.Start = 0
		chunk.End = chunkSize
	} else {
		chunk.Start = lastEnd
		chunk.End = lastEnd + chunkSize
	}

	err = db.Conn.QueryRow("INSERT INTO chunks (key, startSeed, endSeed, criteria, startTime, seedsFound, verified) VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING id;", chunk.Key, chunk.Start, chunk.End, criteria, time.Now(), nil, false).Scan(&chunk.ID)

	return
}

func (db DB) ChunkDone(chunk util.Chunk) (err error) {
	var seedsFound int
	err = db.Conn.QueryRow("SELECT seedsFound FROM chunks WHERE id = $1 AND verifier = $2;", chunk.ID, chunk.Key).Scan(&seedsFound)
	if err != nil && err != sql.ErrNoRows {
		return
	}
	var result sql.Result
	if err == sql.ErrNoRows {
		result, err = db.Conn.Exec("UPDATE chunks SET seedsFound = $1 WHERE id = $2 AND key = $3 AND startSeed = $4 AND endSeed = $5;", chunk.SeedsFound, chunk.ID, chunk.Key, chunk.Start, chunk.End)
		if err != nil {
			err = fmt.Errorf("Chunk: %d, Err: %w", chunk.ID, err)
			return
		}
		var rowsAff int64
		rowsAff, err = result.RowsAffected()
		if rowsAff < 1 || err != nil {
			if err == nil {
				var r []interface{}
				db.Conn.QueryRow("SELECT * FROM chunks WHERE id = $1;", chunk.ID).Scan(&r)
				err = fmt.Errorf("FIRST SEARCH DONE Database error, ID: %d\n%s", chunk.ID, r)
			}
			return
		}
		return
	}
	if seedsFound != chunk.SeedsFound {
		result, err = db.Conn.Exec("DELETE FROM chunks WHERE id = $1 AND verifier = $2 AND startSeed = $3 AND endSeed = $4;", chunk.ID, chunk.Key, chunk.Start, chunk.End)
		if err != nil {
			err = fmt.Errorf("Chunk: %d, Err: %w", chunk.ID, err)
			return
		}
		var rowsAff int64
		rowsAff, err = result.RowsAffected()
		if rowsAff < 1 || err != nil {
			if err == nil {
				err = fmt.Errorf("DELETE Database error")
			}
			return
		}
		return
	}
	log.Println("Setting verified: ", chunk)
	result, err = db.Conn.Exec("UPDATE chunks SET verified = TRUE WHERE id = $1 AND verifier = $2 AND startSeed = $3 AND endSeed = $4 AND seedsFound = $5;", chunk.ID, chunk.Key, chunk.Start, chunk.End, chunk.SeedsFound)
	if err != nil {
		err = fmt.Errorf("Chunk: %d, Err: %w", chunk.ID, err)
		return
	}
	var rowsAff int64
	rowsAff, err = result.RowsAffected()
	if rowsAff < 1 || err != nil {
		if err == nil {
			err = fmt.Errorf("VERIFIED Database error")
		}
		return
	}
	return
}

func (db DB) SystemKeyExists(key string) (err error) {
	var outKey string
	err = db.Conn.QueryRow("SELECT key FROM searchers WHERE key = $1;", key).Scan(&outKey)
	if err != nil {
		return
	}
	if outKey != key {
		return fmt.Errorf("Key validation error")
	}
	return
}

func (db DB) SystemAnalyzeKeyExists(key string) (err error) {
	var outKey string
	err = db.Conn.QueryRow("SELECT key FROM analyzers WHERE key = $1;", key).Scan(&outKey)
	if err != nil {
		return
	}
	if outKey != key {
		return fmt.Errorf("Key validation error")
	}
	return
}

func (db DB) CleanupChunks() (err error) {
	killPoint := time.Now().Add(-12 * time.Hour)
	_, err = db.Conn.Exec("DELETE FROM chunks WHERE seedsFound IS NULL AND startTime < $1;", killPoint)
	if err != nil {
		log.Println(fmt.Errorf("Cleanup err: %w", err))
	}
	_, err = db.Conn.Exec("UPDATE chunks SET verStartTime = NULL, verifier = NULL WHERE seedsFound >= 0 AND verified = FALSE AND verStartTime < $1;", killPoint)
	if err != nil {
		log.Println(fmt.Errorf("Cleanup verification err: %w", err))
	}
	return
}

func (db DB) FalsePositive(seed util.Seed) (err error) {
	_, err = db.Conn.Exec("DELETE FROM seeds WHERE seed = $1;", seed.Seed)
	if err != nil {
		err = fmt.Errorf("False positive err: %w", err)
	}
	return
}

func (db DB) UniqueKeysOverPerios() (unique int, err error) {
	err = db.Conn.QueryRow("SELECT count(DISTINCT fo.key) FROM (SELECT key FROM chunks WHERE startTime > '$1' UNION ALL SELECT verifier as key FROM chunks WHERE verStartTime > '$1') AS fo;", time.Now().Add(time.Hour)).Scan(&unique)
	if err != nil {
		return
	}
	return
}
