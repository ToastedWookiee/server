package db

import (
	"gitlab.com/controllercubiomes/util"
)

func sumOcean(g util.Ocean) float32 {
	return g.Cold_ocean + g.Deep_cold_ocean + g.Deep_frozen_ocean + g.Deep_lukewarm_ocean + g.Deep_ocean + g.Deep_warm_ocean + g.Frozen_ocean + g.Frozen_river + g.Lukewarm_ocean + g.Ocean + g.Warm_ocean
}

func sumMesa(g util.Mesa) float32 {
	return g.Badlands + g.Badlands_plateau + g.Eroded_badlands + g.Modified_badlands_plateau + g.Modified_wooded_badlands_plateau + g.Wooded_badlands_plateau
}

func sumDesert(g util.Desert) float32 {
	return g.Desert + g.Desert_hills + g.Desert_lakes
}

func sumSavanna(g util.Savanna) float32 {
	return g.Savanna + g.Savanna_plateau + g.Shattered_savanna + g.Shattered_savanna_plateau
}

func sumPlains(g util.Plains) float32 {
	return g.Plains + g.Sunflower_plains
}

func sumSwamp(g util.Swamp) float32 {
	return g.Swamp + g.Swamp_hills
}

func sumForest(g util.Forest) float32 {
	return g.Birch_forest + g.Birch_forest_hills + g.Flower_forest + g.Forest + g.Tall_birch_forest + g.Tall_birch_hills + g.Wooded_hills
}

func sumRoofed(g util.Roofed) float32 {
	return g.Dark_forest + g.Dark_forest_hills
}

func sumMountain(g util.Mountain) float32 {
	return g.Gravelly_mountains + g.Modified_gravelly_mountains + g.Mountain_edge + g.Mountains + g.Wooded_mountains
}

func sumMushroom(g util.Mushroom) float32 {
	return g.Mushroom_fields + g.Mushroom_field_shore
}

func sumJungle(g util.Jungle) float32 {
	return g.Bamboo_jungle + g.Bamboo_jungle_hills + g.Jungle + g.Jungle_edge + g.Jungle_hills + g.Modified_jungle + g.Modified_jungle_edge
}

func sumTaiga(g util.Taiga) float32 {
	return g.Snowy_taiga + g.Snowy_taiga_hills + g.Snowy_taiga_mountains + g.Taiga + g.Taiga_hills + g.Taiga_mountains
}

func sumMegaTaiga(g util.MegaTaiga) float32 {
	return g.Giant_spruce_taiga + g.Giant_spruce_taiga_hills + g.Giant_tree_taiga + g.Giant_tree_taiga_hills
}

func sumSnowy(g util.Snowy) float32 {
	return g.Ice_spikes + g.Snowy_beach + g.Snowy_mountains + g.Snowy_taiga + g.Snowy_taiga_hills + g.Snowy_taiga_mountains + g.Snowy_tundra
}
